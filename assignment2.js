class Person {
    constructor(name, age, salary, sex){
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }

    sort(arr, sorting_field, sorting_type){
        let new_arr = [...arr]
        this.quickSort(new_arr,sorting_field);

        if(sorting_type === "asc"){
            return new_arr;
        } else if (sorting_type === "desc") {
            return new_arr.reverse();
        } else {
            return "Enter either asc or dsc!"
        }
    }

    quickSort(array, key) {
        for (var i = 0; i < array.length; i++) 
            { 
                var currVal = array[i][key]; 
                var currElem = array[i]; 
                var j = i - 1; 

                while ((j >= 0) && (array[j][key] > currVal)) {
                    array[j + 1] = array[j];
                    j--;
                }
                
                array[j + 1] = currElem;
            }
    }
}

let p1 = new Person('Utkarsh', 21, 5000, 'Male');
let p2 = new Person('Sneha', 20, 7000, 'Female');
let p3 = new Person('Vedant', 22, 10000, 'Male');
let p4 = new Person('Ayush', 25, 9000, 'Male');
let p5 = new Person('Ujjwal', 30, 15000, 'Male');

let arr = [
    p1,
    p2,
    p3,
    p4,
    p5,
];

let sorted_arr = p1.sort(arr, 'salary', 'desc');
console.log(sorted_arr);

